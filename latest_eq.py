from google.appengine.ext import webapp
from google.appengine.ext.webapp import template

from xml import sax
from urllib2 import urlopen

import logging
import os
import simplejson

class LatestEqPage(webapp.RequestHandler):
    def get(self):
        latest_eq_url = "http://www.bmkg.go.id/webXML/en_autogempa.xml"
        latest_eq = {}        
        
        #parse the latest earthquake
        parser = sax.make_parser()
        parserhandler = LatestEarthquakeParserHandler()
        parser.setContentHandler(parserhandler)
        parser.parse(urlopen(latest_eq_url))
        latest_eq = LatestEarthquakeParserHandler.getEarthquake(parserhandler)        
        
        #fallback mechanism for browser that does not support HTML5 Geolocation
        fallbackCountry = self.request.headers["X-AppEngine-Country"]
        fallbackRegion = self.request.headers["X-AppEngine-Region"]
        fallbackCity = self.request.headers["X-AppEngine-City"]
        fallbackLat, fallbackLong = self.request.headers["X-AppEngine-CityLatLong"].split(",")

        template_values = {
           'latestMagnitude' : latest_eq["magnitude"],
           'latestTsunamiStatus' : latest_eq["tsunami_stat"],
           'latestDepth' : latest_eq["depth"],
           'latestDate' : latest_eq["date"],
           'latestTime' : latest_eq["time"],
           'latest_eq_latitude' : latest_eq["latitude"],
           'latest_eq_longitude' : latest_eq["longitude"],
           'fallbackCountry' : fallbackCountry,
           'fallbackRegion' : fallbackRegion,
           'fallbackCity' : fallbackCity,
           'fallbackLat' : fallbackLat,
           'fallbackLong' : fallbackLong
        }

        path = os.path.join(os.path.dirname(__file__), 'templates/latest_eq.html')
        self.response.out.write(template.render(path, template_values))

class LatestEarthquakeParserHandler(sax.ContentHandler):
    def __init__(self):
        self.data = ""
        self.earthquake = {}
    
    def characters(self, content):
        self.data = content
    
    def endElement(self, name):
        if (name == "Tanggal"):
            self.earthquake["date"] = self.data;
        elif (name == "Jam"):
            self.earthquake["time"] = self.data + " (GMT +7)"
        elif (name == "coordinates"):
            l = self.data.split(',')
            self.earthquake["latitude"] = l[0];
            self.earthquake["longitude"] = l[1];
        elif (name == "Magnitude"):
            self.earthquake["magnitude"] = self.data;
        elif (name == "Kedalaman"):
            self.earthquake["depth"] = self.data
        elif (name == "Wilayah1"):
            self.earthquake["area1"] = self.data
        elif (name == "Wilayah2"):
            self.earthquake["area2"] = self.data
        elif (name == "Wilayah3"):
            self.earthquake["area3"] = self.data
        elif (name == "Wilayah4"):
            self.earthquake["area4"] = self.data
        elif (name == "Wilayah5"):
            self.earthquake["area5"] = self.data
        elif (name == "Potensi"):
            self.earthquake["tsunami_stat"] = self.data
    
    @staticmethod
    def getEarthquake(self):
        return self.earthquake
