from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app

from prev_eq import PrevEqPage
from latest_eq import LatestEqPage 

application = webapp.WSGIApplication([('/', LatestEqPage), ('/prev_eq', PrevEqPage)], debug=True)

def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()
