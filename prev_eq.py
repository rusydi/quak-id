from google.appengine.ext import webapp
from google.appengine.ext.webapp import template

from xml import sax
from urllib2 import urlopen

import os
import simplejson

class PrevEqPage(webapp.RequestHandler):
    def get(self):
        list_eq_url = "http://www.bmkg.go.id/webXML/en_gempaterkini.xml"
        list_eq = []

        #parse list of earthquakes
        parser = sax.make_parser()
        parserhandler = EarthquakeParserHandler()
        parser.setContentHandler(parserhandler)
        parser.parse(urlopen(list_eq_url))
        list_eq = EarthquakeParserHandler.getEarthquake(parserhandler)

        template_values = {
            'list_eq' : list_eq        
        }

        path = os.path.join(os.path.dirname(__file__), 'templates/prev_eq.html')
        self.response.out.write(template.render(path, template_values))

class EarthquakeParserHandler(sax.ContentHandler):
    def __init__(self):
        self.data = ""
        self.earthquake = {}
        self.list_earthquake = []
    
    def characters(self, content):
        self.data = content
    
    def endElement(self, name):
        if (name == "gempa"):
            self.list_earthquake.append(self.earthquake);
            self.earthquake = {}
        elif (name == "Tanggal"):
            self.earthquake["date"] = self.data
        elif (name == "Jam"):
            self.earthquake["time"] = self.data + " (GMT +7)"
        elif (name == "coordinates"):
            l = self.data.split(',')
            self.earthquake["latitude"] = l[0];
            self.earthquake["longitude"] = l[1];
        elif (name == "Magnitude"):
            self.earthquake["magnitude"] = self.data
        elif (name == "Kedalaman"):
            self.earthquake["depth"] = self.data
        elif (name == "Wilayah"):
            self.earthquake["area"] = self.data

    @staticmethod
    def getEarthquake(self):
        return self.list_earthquake

